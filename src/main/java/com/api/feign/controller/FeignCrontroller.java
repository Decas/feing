package com.api.feign.controller;


import com.api.feign.cliente.IClienteFeing;
import com.api.feign.model.entity.Item;
import com.api.feign.model.entity.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FeignCrontroller {

    @Autowired
    public IClienteFeing clienteFeing;

   @GetMapping("/listProductos")
    public List<Producto> find() {
        return this.clienteFeing.find().stream().map(p-> new Producto(p.getId(),p.getNombre(),p.getDescripcion(),p.getPrecio())).collect(Collectors.toList());
    }

    @GetMapping("/listatId/{id}")
    public Item buscarId(@PathVariable long id){

       Item i=new Item(1,this.clienteFeing.buscarId(id));
       return i;

    }




}

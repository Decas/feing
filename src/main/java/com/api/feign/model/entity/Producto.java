package com.api.feign.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Producto {

    private  long id;
    private String nombre;
    private String descripcion;
    private double precio;
}

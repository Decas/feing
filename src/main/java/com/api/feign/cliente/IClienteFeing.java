package com.api.feign.cliente;


import com.api.feign.model.entity.Item;
import com.api.feign.model.entity.Producto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "api-producto", url = "http://localhost:9000")
public interface IClienteFeing {

    @GetMapping("/listProductos")
    public List<Producto> find();

    @GetMapping("/listatId/{id}")
    public Producto buscarId(@PathVariable long id);
}
